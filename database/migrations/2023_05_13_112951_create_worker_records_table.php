<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('worker_records', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id');
            $table->string('worker_name');
            $table->integer('hours');
            $table->float('rate');
            $table->boolean('taxable')->default(false);
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('shift_type');
            $table->boolean('paid')->default(false);
            $table->date('paid_at')->nullable()->default(null);
            $table->timestamps();

            $table->index(['company_id', 'worker_name'], 'worker_co_fk');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');
            $table->foreign('shift_type')->references('id')->on('shifts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {   
        Schema::dropIfExists('worker_records');
    }
};
