<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Reader;
use App\Models\Company;
use App\Models\Shift;
use App\Models\Worker;
use App\Models\Status;
use App\Models\WorkerRecord;

class MainController extends Controller
{   
    public function compare($a, $b) {
        return strtotime($a['Date']) - strtotime($b['Date']);
    }

    public function createShift(Request $request) {
        // just for protection purpose
        if ($request->filled('worker_name') &&
            $request->filled('company') &&
            $request->filled('hours') &&
            $request->filled('rate') &&
            $request->filled('taxable') &&
            $request->filled('status_id') &&
            $request->filled('shift_type') &&
            $request->filled('paid') &&
            $request->filled('paid_at') &&
            $request->filled('created_at')) {
            
            $workerRecord = new WorkerRecord();
            $workerRecord->worker_name = $request->input('worker_name');
            $workerRecord->hours = $request->input('hours');
            $workerRecord->rate = $request->input('rate');
            $workerRecord->company_id = $request->input('company');
            $workerRecord->taxable = $request->input('taxable');
            $workerRecord->status_id = $request->input('status_id');
            $workerRecord->shift_type = $request->input('shift_type');
            $workerRecord->paid = $request->input('paid');
            $workerRecord->paid_at = $request->input('paid_at');
            $workerRecord->created_at = $request->input('created_at');
            $workerRecord->save();

            $worker = new Worker();
            $worker->insertOrIgnore(['company_id' => $request->input('company'), 'name' => $request->input('worker_name')]);
            
            return response()->json(['message' => 'Shift was successfully created.'], 200);

        } else {
            return response()->json(['error' => 'One or more inputs are missing or empty.'], 400);
        }
    }

    public function uploadFile(Request $request) {

        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        try {
            if ($request->hasFile('file') && $extension === 'csv') {
                $fileName = $file->getClientOriginalName();
                $filePath = $file->storeAs('uploads', $fileName);
            
                // Parse the uploaded file here
                $csv = Reader::createFromPath(storage_path('app/' . $filePath), 'r');
                $csv->setHeaderOffset(0);
                $data = $csv->getRecords();

                $finalData = [];
                $statusData = [];
                $shiftsData = [];

                // Insert data into database
                foreach ($data as $offset => $record) {
                    if (!in_array($record['Status'], $statusData)) { 
                        array_push($statusData, $record['Status']);
                    }

                    if (!in_array($record['Shift Type'], $shiftsData)) { 
                        array_push($shiftsData, $record['Shift Type']);
                    }
                    
                    if (!array_key_exists($record['Company'], $finalData)) { 
                        $finalData[$record['Company']] = [];
                    }

                    if (!array_key_exists($record['Worker'], $finalData[$record['Company']])) { 
                        $finalData[$record['Company']][$record['Worker']] = [];
                    }

                    $workerData = $record;

                    unset($workerData['Company']);
                    unset($workerData['Worker']);

                    array_push($finalData[$record['Company']][$record['Worker']], $workerData);
                }

                // Sort the companies by alphabetical order
                ksort($finalData);

                // Iterate to sort inside nested arrays
                foreach ($finalData as $key => $val) {
                    // For every company we use this associative array by alphabetical order of the key (in this case name of the worker)
                    ksort($finalData[$key]);

                    // Iterate every record for every worker and use a custom sorting function to sort it by Date
                    foreach ($finalData[$key] as $k => $v) {
                        usort($finalData[$key][$k], function($a, $b) {
                            return $this->compare($a, $b);
                        });
                    }
                }   

                foreach ($statusData as $status) {
                    Status::insertOrIgnore(['status' => $status]);
                }

                foreach ($shiftsData as $shift) {
                    Shift::insertOrIgnore(['shift' => $shift]);
                }

                // Iterating finalData
                foreach ($finalData as $key => $value) {
                    // populate companies table
                    $companyId = Company::insertGetId(['name' => $key]);

                    foreach ($value as $k => $v) {
                        $shiftRecords = [];

                        // Populate workers table
                        Worker::insertOrIgnore([
                            'company_id' => $companyId,
                            'name' => $k,
                        ]);

                        $workerName = $k;

                        // Start inserting data to worker_records table
                        foreach ($v as $workerRecord) {

                            // Get the ids from the tables we need
                            $status = Status::where('status', $workerRecord['Status'])->first();
                            $status_id = $status->id;
                        
                            $shift = Shift::where('shift', $workerRecord['Shift Type'])->first();
                            $shift_id = $shift->id;
                        
                            // Determine if the worker record is taxable
                            $taxable = $workerRecord['Taxable'] === 'Yes' ? 1 : 0;
                        
                            // Determine if the worker record is paid
                            $paid = $workerRecord['Paid At'] !== '' ? 1 : 0;

                            $rate_per_hour = floatval(ltrim($workerRecord['Rate per Hour'], '£'));

                            array_push($shiftRecords, [
                                'company_id' => $companyId,
                                'worker_name' => $workerName,
                                'hours' => $workerRecord['Hours'],
                                'rate' => $rate_per_hour,
                                'taxable' => $taxable,
                                'status_id' => $status_id,
                                'shift_type' => $shift_id,
                                'paid' => $paid,
                                'paid_at' => $workerRecord['Paid At'],
                                'created_at' => $workerRecord['Date']
                            ]);
                        }
                        
                        // Insert the worker record
                        WorkerRecord::insertOrIgnore($shiftRecords);
                    }
                }

                return response()->json(['message' => 'File uploaded and data inserted successfully'], 200);
            }

            return response()->json(['error' => 'File failed to upload. Please make sure that the uploaded file is of CSV file type.'], 500);
        }
        catch(Exception $e) {
            throw $e;
        }
    }

    public function getEmployees(Request $request) {
        $query = Worker::query()->with(['company' => function($q) {
            $q->select('id', 'name');
        }]);

        // Count the total number of records
        $total_records = $query->count();
        
        // Paginate the results
        $per_page = $request->input('pageSize', 10);
        $page = $request->input('page', 1);
        $offset = ($page - 1) * $per_page;
        
        $items = $query
            ->orderBy('name', 'asc')
            ->skip($offset)
            ->take($per_page)
            ->get()
            ->map(function ($item) {
                return [
                    'name' => $item->name,
                    'company_name' => $item->company->name,
                    'company_id' => $item->company->id,
                ];
            })
            ->toArray();

        // Return the response
        return response()->json([
            'items' => $items,
            'total_records' => $total_records,
            'per_page' => $per_page,
            'current_page' => $page,
        ]);
    }

    public function getEmployeeSummary(Request $request, $companyId, $workerName) {

        $q1 = WorkerRecord::where('company_id', $companyId)->where('worker_name', $workerName);
        $workerTotal = $q1->count();

        $q2 = WorkerRecord::selectRaw("worker_name, round(sum(rate)/ $workerTotal ,2) as avg_pay, round(sum(rate*hours) / $workerTotal, 2) as total_avg_pay")
            ->where('worker_name', $workerName)
            ->where('company_id', $companyId)
            ->groupBy('worker_name')
            ->get();

        $q3 = WorkerRecord::with(['company', 'status' => function($q){
                $q->where('status', 'complete');
            }])->where('company_id', $companyId)
            ->where('worker_name', $workerName)
            ->where('paid_at', '!=', '0000-00-00')
            ->orderBy('paid_at',  'DESC',)
            ->limit(5)
            ->get();

        $recentShifts = [];
        
        return response()->json([
            'worker_name' => $q3[0]->worker_name,
            'average_pay' => $q2[0]->avg_pay,
            'total_average_pay' => $q2[0]->total_avg_pay,
            'recent_shifts' => $q3,
        ]);
    }

    public function shifts(Request $request) {
        $q = WorkerRecord::with('company', 'status', 'shift')
            ->select('*', \DB::raw('(rate * hours) as total_pay'))
            ->orderBy('company_id')
            ->orderBy('worker_name')
            ->orderBy('created_at', 'DESC');

        // Count the total number of records
        $total_records = $q->count();
        
        // Paginate the results
        $per_page = $request->input('pageSize', 10);
        $page = $request->input('page', 1);
        $offset = ($page - 1) * $per_page;

        // Apply filters
        if ($request->has('searchTerm') && $request->input('searchTerm') != '') {
            $search = $request->input('searchTerm');
            $q->havingRaw('total_pay > ?', [$search]);
            // $q->havingRaw('total_pay like ?', ['%' . $search . '%']);
        }

        // Get the paginated records
        $items = $q->skip($offset)
            ->take($per_page)
            ->get()
            ->toArray();

        
        return response()->json([
            'items' => $items,
            'total_records' => $total_records,
            'per_page' => $per_page,
            'current_page' => $page,
        ]);
    }

    public function getShiftData(Request $request, $id) {

        $workerRecord = WorkerRecord::findOrFail($id);

        return response()->json([
            'records' => $workerRecord,
        ], 200);
    }

    public function editShift(Request $request, $id) {
        $shiftId = $id;

        // just for protection purpose
        if ($request->filled('worker_name') &&
            $request->filled('company') &&
            $request->filled('hours') &&
            $request->filled('rate') &&
            $request->filled('taxable') &&
            $request->filled('status_id') &&
            $request->filled('shift_type') &&
            $request->filled('paid') &&
            $request->filled('paid_at') &&
            $request->filled('created_at')) {
            
            $workerRecord = WorkerRecord::findOrFail($shiftId);
            $workerRecord->worker_name = $request->input('worker_name');
            $workerRecord->hours = $request->input('hours');
            $workerRecord->rate = $request->input('rate');
            $workerRecord->company_id = $request->input('company');
            $workerRecord->taxable = $request->input('taxable');
            $workerRecord->status_id = $request->input('status_id');
            $workerRecord->shift_type = $request->input('shift_type');
            $workerRecord->paid = $request->input('paid');
            $workerRecord->paid_at = $request->input('paid_at');
            $workerRecord->created_at = $request->input('created_at');
            $workerRecord->save();
            
            $worker = new Worker();
            $worker->updateOrInsert(['company_id' => $request->input('company'), 'name' => $request->input('worker_name')]);

            return response()->json(['message' => 'Shift was successfully updated.'], 200);

        } else {
            return response()->json(['error' => 'One or more inputs are missing or empty.'], 400);
        }
        dd($request->all());
    }

    public function destroy(Request $request, $id) {
        $wr = WorkerRecord::where('id', $id)->first();
        $wr->delete();
        return;
    }

    public function statusData() {
        try {
            $statuses = Status::all()->toArray();
            return response()->json([
                'statuses' => $statuses,
            ]);
        } catch(\Exception $e) {
            throw $e;
        }
    }

    public function shiftTypeData() {
        try {
            $shifts = Shift::all()->toArray();
            return response()->json([
                'shift_types' => $shifts,
            ]);
        } catch(\Exception $e) {
            throw $e;
        }  
    }

    public function companies() {
        try {
            $companies = Company::all()->toArray();
            return response()->json([
                'companies' => $companies,
            ]);
        } catch(\Exception $e) {
            throw $e;
        }  
    }
}
