<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\WorkerRecord;
use App\Models\Worker;

class Company extends Model
{
    use HasFactory;

    // Added this to allow mass assignment
    protected $fillable = ['name'];

    // Exclude timestamps
    public $timestamps = false;

    public function workerRecords()
    {
        return $this->hasMany(WorkerRecord::class);
    }

    public function workers()
    {
        return $this->hasMany(Worker::class);
    }
}
