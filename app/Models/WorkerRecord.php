<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\Shift;
use App\Models\Status;
use App\Models\Worker;

class WorkerRecord extends Model
{
    use HasFactory;

    // Exclude timestamps
    // public $timestamps = false;

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function worker()
    {
        return $this->belongsTo(Worker::class, 'worker_name', 'name')
            ->whereColumn('worker_records.company_id', 'workers.company_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function shift()
    {
        return $this->belongsTo(Status::class);
    }
}
