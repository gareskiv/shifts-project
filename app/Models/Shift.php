<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\WorkerRecord;

class Shift extends Model
{
    use HasFactory;
    
    // Exclude timestamps
    public $timestamps = false;

    public function workerRecords()
    {
        return $this->belongsTo(Status::class);
    }
}
