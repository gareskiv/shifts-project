<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\WorkerRecord;

class Worker extends Model
{
    use HasFactory;

    // Exclude timestamps
    public $timestamps = false;
    
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function workerRecords()
    {
        return $this->hasMany(WorkerRecord::class);
    }
}
