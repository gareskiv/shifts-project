<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/all-employees',                        [MainController::class, 'getEmployees']);
Route::get('/employee/{companyId}/{workerName}',    [MainController::class, 'getEmployeeSummary']);
Route::get('/all-shifts',                           [MainController::class, 'shifts']);
Route::get('/get-shifts',                           [MainController::class, 'shiftTypeData']);
Route::get('/get-statuses',                         [MainController::class, 'statusData']);
Route::get('/get-companies',                        [MainController::class, 'companies']);
Route::get('/get-shift/{id}',                       [MainController::class, 'getShiftData']);


Route::post('/upload-file',                         [MainController::class, 'uploadFile']);
Route::post('/create-shift',                        [MainController::class, 'createShift']);
Route::post('/edit-shift/{id}',                     [MainController::class, 'editShift']);

Route::delete('/delete-employee/{id}',              [MainController::class, 'destroy']);







