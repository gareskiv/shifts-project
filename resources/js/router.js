import { createRouter, createWebHistory } from 'vue-router';
import Home from './components/Home.vue';
import Employees from './components/Employees.vue';
import Shifts from './components/Shifts.vue';
import ViewEmployee from './components/crud/ViewEmployee.vue';
import CreateShift from './components/crud/CreateShift.vue';
import EditShift from './components/crud/EditShift.vue';
import PageNotFound from './components/PageNotFound.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/all-employees',
    name: 'employees',
    component: Employees
  },
  {
    path: '/all-shifts',
    name: 'shifts',
    component: Shifts
  },
  {
    path: '/view-employee/:id/:name',
    name: 'view-employee',
    component: ViewEmployee
  },
  {
    path: '/create-employee',
    name: 'create-employee',
    component: CreateShift
  },
  {
    path: '/edit-employee/:id',
    name: 'edit-employee',
    component: EditShift
  },
  { 
    path: '/:pathMatch(.*)*',
    name: 'page-not-found',
    component: PageNotFound 
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;