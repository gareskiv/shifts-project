<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Shifts</title>
        @vite(['resources/js/app.js', 'resources/scss/app.scss'])
    </head>
    <body>
        <div id="app"></div>
    </body>
</html>
