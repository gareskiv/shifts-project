In this project, I am using Laravel 10 and Vue3.

To get started, clone the project into your local repository and navigate to the project location. Then, run "composer install" and "npm install" to install the dependencies.

After you install composer and node package manager you should run 'npm run dev ' and 'php artisan serve'.

I also have attached the .env file.

Note: The query for importing the shifts.csv file can be slow. It can be improved with Lazy collections, but it was not implemented due to time constraints. I apologize for any inconvenience this may cause.

If you have any issues or questions, please don't hesitate to reach out to me.